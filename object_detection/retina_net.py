from d3m.container.numpy import ndarray
from d3m.container.pandas import DataFrame
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, GradientCompositionalityMixin, Gradients, DockerContainer, ContinueFitMixin
from fastai.vision.models.unet import _get_sfs_idxs, model_sizes, hook_outputs
#import common_primitives
import collections
from collections import OrderedDict
from d3m.container.dataset import Dataset
#from .utils import to_variable
from typing import Dict, List, Tuple, Type, Collection
# from functools import reduce
# from operator import mul
import os
import math
import sys
from fastai.vision import ObjectItemList, get_transforms, bb_pad_collate, Sizes, LossFunction, create_body, models, \
                            conv2d, conv_layer, ifnone, Learner, is_tuple, FloatTensor, tensor, LongTensor, ImageList, \
                            range_of
from torch import nn
import torch.nn.functional as F
import fastai
import torch
import numpy as np
import pandas as pd
# import abc
# import time
# import numpy as np  # type: ignore
# import torch  # type: ignore
# import torch.nn as nn  # type: ignore
# import torch.nn.functional as F  # type: ignore
# import torch.optim as optim  # type: ignore
# from torch.autograd import Variable  # type: ignore

Inputs = DataFrame
Outputs = DataFrame

class Params(params.Params):
    state: Dict


class Hyperparams(hyperparams.Hyperparams):

    # dataset = hyperparams.Hyperparameter[Dataset](
    #     semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
    #     default=Dataset(),
    #     description='Dataset associated with the input data.'
    # )

    batch_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=64,
        description='Batch size.'
    )

    img_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=128,
        description='Image size.'
    )

    learning_rate = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1e-4,
        description='Learning rate used during training (fit).'
    )

    detect_threshold = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.99,
        description='Detection threshold for how confident our predictions are.'
    )

    nms_threshold = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.25,
        description='Threshold for nms process of filtering down of overlapping bounding boxes.'
    )

    use_gpu = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=True,
        description='Whether or not to use GPU for compute. This should ALWAYS be turned on.'
    )

    # Default hyperparameters for general primitive behavior
    use_input_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
    )
    use_output_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_input_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
    )
    exclude_output_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )
    add_index_columns = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    error_on_no_input = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False.",
    )



class RetinaNet(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams], ContinueFitMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    Implementation of RetinaNet for object detection https://arxiv.org/abs/1708.02002
    """

    __author__ = 'Mark Hoffmann <mark.k.hoffmann@jpl.nasa.gov>'
    metadata = metadata_module.PrimitiveMetadata({
        'id': '176e7277-fe37-402b-86e6-bdb7f7a9cf89',
        'version': '0.1.0',
        'name': 'retina_net',
        'keywords': ['neural network', 'deep learning', 'object detection'],
        'source': {
            'name': 'Mark Hoffmann',
            'contact': 'mailto:mark.k.hoffmann@jpl.nasa.gov',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/object_detection/blob/master/object_detection/retina_net.py',
                'https://gitlab.com/datadrivendiscovery/object_detection.git',
            ],
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/object_detection.git@{git_commit}#egg=object_detection'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.object_detection.retina_net.ObjectDetection',
        'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
        'primitive_family': 'OBJECT_DETECTION',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        fastai.torch_core.defaults.device = torch.device('cuda') if self.hyperparams['use_gpu'] else torch.device('cpu')

    def _get_annotations_from_d3m_dataframe(self, df):

        def get_categories_from_dataframe(df, class_col_name):
            try:
                unique_classes = df[class_col_name].unique().tolist()
                cat_list = []
                for idx, c in enumerate(unique_classes):
                    cat_list.append({"id":idx+1,"name":c})
            except:
                cat_list = [{"id":1,"name":"cat1"}]
            return cat_list

        def get_images_from_dataframe(df, image_col_name):
            unique_photos = df[image_col_name].unique().tolist()
            images = []
            for idx, photo in enumerate(unique_photos):
                images.append({"file_name":photo,"id":idx})
            return images

        def lookup_id_by_key(match, key, l):
            return [d["id"] for d in l if d[key] == match][0]


        def convert_polygon_coords_to_coco_format(coords):
            # d3m format --> coco format
            #"x11,y11,x12,y12,x13,y13,x14,y14" --> (x, y, width, height)
            """
            TODO: Need to come back to this in order to do generic from Polygon. This is a short cut because we know for
            the object detection tasks in d3m as they currently stand we are storing as "polygon" even though we know it's really
            just a box with 4 coordinates...
            """
            points = [int(v) for v in coords.split(",")]
            x = min(points[::2])
            y = min(points[1::2])
            width = abs(points[0] - points[4])
            height = abs(points[1] - points[5])
            return [x, y, width, height]

        def get_annotations_from_dataframe(df, class_col_name, image_col_name, bbox_col_name):
            # if not self.hyperparams['use_semantic_types']:
            #     return
            # error statements if more than 1 column with that semantic type
            #print(df.metadata.pretty_print())
            #print(df.metadata.pretty_print(), file=sys.__stdout__)

            #print("col names:", file=sys.__stdout__)
            #print(f"{bbox_col_name} : {image_col_name} : {class_col_name}", file=sys.__stdout__)
            annotations = []
            #print(df.head(), file=sys.__stdout__)
            for idx, row in df.iterrows():
                current_annot = {}
                if row[image_col_name] == "":
                    break
                try:
                    current_annot["category_id"] = lookup_id_by_key(row[class_col_name], "name", get_categories_from_dataframe(df, class_col_name))
                except:
                    current_annot["category_id"] = 1
                current_annot["image_id"] = lookup_id_by_key(row[image_col_name], "file_name", get_images_from_dataframe(df, image_col_name))
                #print(row, file=sys.__stdout__)
                current_annot["bbox"] = convert_polygon_coords_to_coco_format(row[bbox_col_name])
                annotations.append(current_annot)
            return annotations

        # Very Very bad way to do this, come back to fix later...
        try:
            self._bbox_col_name = df.columns[df.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/BoundingPolygon')][0]
        except:
            self._bbox_col_name = "bounding_box"
        try:
            self._image_col_name = df.columns[df.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/Text')][0]
        except:
            self._image_col_name = "filename"
        try:
            self._class_col_name = df.columns[df.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/TrueTarget')][0]
            if self._class_col_name == bbox_col_name:
                self._class_col_name = "class"
        except:
            self._class_col_name = "class"

        #Process D3M to standard COCO form
        annot_dict = {"categories":[],"annotations":[], "images":[]}
        annot_dict["categories"] = get_categories_from_dataframe(df, self._class_col_name)
        annot_dict["annotations"] = get_annotations_from_dataframe(df, self._class_col_name, self._image_col_name, self._bbox_col_name)
        annot_dict["images"] = get_images_from_dataframe(df, self._image_col_name)

        # Return processed info
        id2images, id2bboxes, id2cats = {}, collections.defaultdict(list), collections.defaultdict(list)
        classes = {}
        #print(annot_dict['categories'], file=sys.__stdout__)
        for o in annot_dict['categories']:
            classes[o['id']] = o['name']
        for o in annot_dict['annotations']:
            #print(o,file=sys.__stdout__)
            bb = o['bbox']
            id2bboxes[o['image_id']].append([bb[1],bb[0], bb[3]+bb[1], bb[2]+bb[0]])
            id2cats[o['image_id']].append(classes[o['category_id']])
        for o in annot_dict['images']:
            if o['id'] in id2bboxes:
                id2images[o['id']] = o['file_name']
        ids = list(id2images.keys())
        return [id2images[k] for k in ids], [[id2bboxes[k], id2cats[k]] for k in ids]

    def _create_learner(self):
        class LateralUpsampleMerge(nn.Module):
            "Merge the features coming from the downsample path (in `hook`) with the upsample path."
            def __init__(self, ch, ch_lat, hook):
                super().__init__()
                self.hook = hook
                self.conv_lat = conv2d(ch_lat, ch, ks=1, bias=True)

            def forward(self, x):
                return self.conv_lat(self.hook.stored) + F.interpolate(x, self.hook.stored.shape[-2:], mode='nearest')

        class RetinaNet(nn.Module):
            "Implements RetinaNet from https://arxiv.org/abs/1708.02002"
            def __init__(self, encoder:nn.Module, n_classes, final_bias=0., chs=256, n_anchors=9, flatten=True):
                super().__init__()
                self.n_classes,self.flatten = n_classes,flatten
                imsize = (256,256)
                sfs_szs = model_sizes(encoder, size=imsize)
                sfs_idxs = list(reversed(_get_sfs_idxs(sfs_szs)))
                self.sfs = hook_outputs([encoder[i] for i in sfs_idxs])
                self.encoder = encoder
                self.c5top5 = conv2d(sfs_szs[-1][1], chs, ks=1, bias=True)
                self.c5top6 = conv2d(sfs_szs[-1][1], chs, stride=2, bias=True)
                self.p6top7 = nn.Sequential(nn.ReLU(), conv2d(chs, chs, stride=2, bias=True))
                self.merges = nn.ModuleList([LateralUpsampleMerge(chs, sfs_szs[idx][1], hook)
                                             for idx,hook in zip(sfs_idxs[-2:-4:-1], self.sfs[-2:-4:-1])])
                self.smoothers = nn.ModuleList([conv2d(chs, chs, 3, bias=True) for _ in range(3)])
                self.classifier = self._head_subnet(n_classes, n_anchors, final_bias, chs=chs)
                self.box_regressor = self._head_subnet(4, n_anchors, 0., chs=chs)

            def _head_subnet(self, n_classes, n_anchors, final_bias=0., n_conv=4, chs=256):
                "Helper function to create one of the subnet for regression/classification."
                layers = [conv_layer(chs, chs, bias=True, norm_type=None) for _ in range(n_conv)]
                layers += [conv2d(chs, n_classes * n_anchors, bias=True)]
                layers[-1].bias.data.zero_().add_(final_bias)
                layers[-1].weight.data.fill_(0)
                return nn.Sequential(*layers)

            def _apply_transpose(self, func, p_states, n_classes):
                #Final result of the classifier/regressor is bs * (k * n_anchors) * h * w
                #We make it bs * h * w * n_anchors * k then flatten in bs * -1 * k so we can contenate
                #all the results in bs * anchors * k (the non flatten version is there for debugging only)
                if not self.flatten:
                    sizes = [[p.size(0), p.size(2), p.size(3)] for p in p_states]
                    return [func(p).permute(0,2,3,1).view(*sz,-1,n_classes) for p,sz in zip(p_states,sizes)]
                else:
                    return torch.cat([func(p).permute(0,2,3,1).contiguous().view(p.size(0),-1,n_classes) for p in p_states],1)

            def forward(self, x):
                c5 = self.encoder(x)
                p_states = [self.c5top5(c5.clone()), self.c5top6(c5)]
                p_states.append(self.p6top7(p_states[-1]))
                for merge in self.merges: p_states = [merge(p_states[0])] + p_states
                for i, smooth in enumerate(self.smoothers[:3]):
                    p_states[i] = smooth(p_states[i])
                return [self._apply_transpose(self.classifier, p_states, self.n_classes),
                        self._apply_transpose(self.box_regressor, p_states, 4),
                        [[p.size(2), p.size(3)] for p in p_states]]

            def __del__(self):
                if hasattr(self, "sfs"): self.sfs.remove()

        def create_grid(size):
            "Create a grid of a given `size`."
            H, W = size if is_tuple(size) else (size,size)
            grid = FloatTensor(H, W, 2)
            linear_points = torch.linspace(-1+1/W, 1-1/W, W) if W > 1 else tensor([0.])
            grid[:, :, 1] = torch.ger(torch.ones(H), linear_points).expand_as(grid[:, :, 0])
            linear_points = torch.linspace(-1+1/H, 1-1/H, H) if H > 1 else tensor([0.])
            grid[:, :, 0] = torch.ger(linear_points, torch.ones(W)).expand_as(grid[:, :, 1])
            return grid.view(-1,2)

        def create_anchors(sizes, ratios, scales, flatten=True):
            "Create anchor of `sizes`, `ratios` and `scales`."
            aspects = [[[s*math.sqrt(r), s*math.sqrt(1/r)] for s in scales] for r in ratios]
            aspects = torch.tensor(aspects).view(-1,2)
            anchors = []
            for h,w in sizes:
                #4 here to have the anchors overlap.
                sized_aspects = 4 * (aspects * torch.tensor([2/h,2/w])).unsqueeze(0)
                base_grid = create_grid((h,w)).unsqueeze(1)
                n,a = base_grid.size(0),aspects.size(0)
                ancs = torch.cat([base_grid.expand(n,a,2), sized_aspects.expand(n,a,2)], 2)
                anchors.append(ancs.view(h,w,a,4))
            return torch.cat([anc.view(-1,4) for anc in anchors],0) if flatten else anchors



        def activ_to_bbox(acts, anchors, flatten=True):
            "Extrapolate bounding boxes on anchors from the model activations."
            if flatten:
                acts.mul_(acts.new_tensor([[0.1, 0.1, 0.2, 0.2]])) #Can't remember where those scales come from, but they help regularize
                centers = anchors[...,2:] * acts[...,:2] + anchors[...,:2]
                sizes = anchors[...,2:] * torch.exp(acts[...,:2])
                return torch.cat([centers, sizes], -1)
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def cthw2tlbr(boxes):
            "Convert center/size format `boxes` to top/left bottom/right corners."
            top_left = boxes[:,:2] - boxes[:,2:]/2
            bot_right = boxes[:,:2] + boxes[:,2:]/2
            return torch.cat([top_left, bot_right], 1)

        def intersection(anchors, targets):
            "Compute the sizes of the intersections of `anchors` by `targets`."
            ancs, tgts = cthw2tlbr(anchors), cthw2tlbr(targets)
            a, t = ancs.size(0), tgts.size(0)
            ancs, tgts = ancs.unsqueeze(1).expand(a,t,4), tgts.unsqueeze(0).expand(a,t,4)
            top_left_i = torch.max(ancs[...,:2], tgts[...,:2])
            bot_right_i = torch.min(ancs[...,2:], tgts[...,2:])
            sizes = torch.clamp(bot_right_i - top_left_i, min=0)
            return sizes[...,0] * sizes[...,1]

        def IoU_values(anchors, targets):
            "Compute the IoU values of `anchors` by `targets`."
            inter = intersection(anchors, targets)
            anc_sz, tgt_sz = anchors[:,2] * anchors[:,3], targets[:,2] * targets[:,3]
            union = anc_sz.unsqueeze(1) + tgt_sz.unsqueeze(0) - inter
            return inter/(union+1e-8)

        def match_anchors(anchors, targets, match_thr=0.5, bkg_thr=0.4):
            "Match `anchors` to targets. -1 is match to background, -2 is ignore."
            matches = anchors.new(anchors.size(0)).zero_().long() - 2
            if targets.numel() == 0: return matches
            ious = IoU_values(anchors, targets)
            vals,idxs = torch.max(ious,1)
            matches[vals < bkg_thr] = -1
            matches[vals > match_thr] = idxs[vals > match_thr]
            #Overwrite matches with each target getting the anchor that has the max IoU.
            #vals,idxs = torch.max(ious,0)
            #If idxs contains repetition, this doesn't bug and only the last is considered.
            #matches[idxs] = targets.new_tensor(list(range(targets.size(0)))).long()
            return matches

        def tlbr2cthw(boxes):
            "Convert top/left bottom/right format `boxes` to center/size corners."
            center = (boxes[:,:2] + boxes[:,2:])/2
            sizes = boxes[:,2:] - boxes[:,:2]
            return torch.cat([center, sizes], 1)

        def bbox_to_activ(bboxes, anchors, flatten=True):
            "Return the target of the model on `anchors` for the `bboxes`."
            if flatten:
                t_centers = (bboxes[...,:2] - anchors[...,:2]) / anchors[...,2:]
                t_sizes = torch.log(bboxes[...,2:] / anchors[...,2:] + 1e-8)
                return torch.cat([t_centers, t_sizes], -1).div_(bboxes.new_tensor([[0.1, 0.1, 0.2, 0.2]]))
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def encode_class(idxs, n_classes):
            target = idxs.new_zeros(len(idxs), n_classes).float()
            mask = idxs != 0
            i1s = LongTensor(list(range(len(idxs))))
            target[i1s[mask],idxs[mask]-1] = 1
            return target

        class RetinaNetFocalLoss(nn.Module):

            def __init__(self, gamma:float=2., alpha:float=0.25,  pad_idx:int=0, scales:Collection[float]=None,
                         ratios:Collection[float]=None, reg_loss:LossFunction=F.smooth_l1_loss):
                super().__init__()
                self.gamma,self.alpha,self.pad_idx,self.reg_loss = gamma,alpha,pad_idx,reg_loss
                self.scales = ifnone(scales, [1,2**(-1/3), 2**(-2/3)])
                self.ratios = ifnone(ratios, [1/2,1,2])

            def _change_anchors(self, sizes:Sizes) -> bool:
                if not hasattr(self, 'sizes'): return True
                for sz1, sz2 in zip(self.sizes, sizes):
                    if sz1[0] != sz2[0] or sz1[1] != sz2[1]: return True
                return False

            def _create_anchors(self, sizes:Sizes, device:torch.device):
                self.sizes = sizes
                self.anchors = create_anchors(sizes, self.ratios, self.scales).to(device)

            def _unpad(self, bbox_tgt, clas_tgt):
                i = torch.min(torch.nonzero(clas_tgt-self.pad_idx))
                return tlbr2cthw(bbox_tgt[i:]), clas_tgt[i:]-1+self.pad_idx

            def _focal_loss(self, clas_pred, clas_tgt):
                encoded_tgt = encode_class(clas_tgt, clas_pred.size(1))
                ps = torch.sigmoid(clas_pred.detach())
                weights = encoded_tgt * (1-ps) + (1-encoded_tgt) * ps
                alphas = (1-encoded_tgt) * self.alpha + encoded_tgt * (1-self.alpha)
                weights.pow_(self.gamma).mul_(alphas)
                clas_loss = F.binary_cross_entropy_with_logits(clas_pred, encoded_tgt, weights, reduction='sum')
                return clas_loss

            def _one_loss(self, clas_pred, bbox_pred, clas_tgt, bbox_tgt):
                bbox_tgt, clas_tgt = self._unpad(bbox_tgt, clas_tgt)
                matches = match_anchors(self.anchors, bbox_tgt)
                bbox_mask = matches>=0
                if bbox_mask.sum() != 0:
                    bbox_pred = bbox_pred[bbox_mask]
                    bbox_tgt = bbox_tgt[matches[bbox_mask]]
                    bb_loss = self.reg_loss(bbox_pred, bbox_to_activ(bbox_tgt, self.anchors[bbox_mask]))
                else: bb_loss = 0.
                matches.add_(1)
                clas_tgt = clas_tgt + 1
                clas_mask = matches>=0
                clas_pred = clas_pred[clas_mask]
                clas_tgt = torch.cat([clas_tgt.new_zeros(1).long(), clas_tgt])
                clas_tgt = clas_tgt[matches[clas_mask]]
                return bb_loss + self._focal_loss(clas_pred, clas_tgt)/torch.clamp(bbox_mask.sum(), min=1.)

            def forward(self, output, bbox_tgts, clas_tgts):
                clas_preds, bbox_preds, sizes = output
                if self._change_anchors(sizes): self._create_anchors(sizes, clas_preds.device)
                n_classes = clas_preds.size(2)
                return sum([self._one_loss(cp, bp, ct, bt)
                            for (cp, bp, ct, bt) in zip(clas_preds, bbox_preds, clas_tgts, bbox_tgts)])/clas_tgts.size(0)

        class SigmaL1SmoothLoss(nn.Module):

            def forward(self, output, target):
                reg_diff = torch.abs(target - output)
                reg_loss = torch.where(torch.le(reg_diff, 1/9), 4.5 * torch.pow(reg_diff, 2), reg_diff - 1/18)
                return reg_loss.mean()

        self._ratios = [1/2,1,2]
        self._scales = [1,2**(-1/3), 2**(-2/3)]

        encoder = create_body(models.resnet50, cut=-2)
        model = RetinaNet(encoder, self._data.c, final_bias=-4)
        crit = RetinaNetFocalLoss(scales=self._scales, ratios=self._ratios)
        learn = Learner(self._data, model, loss_func=crit)

        return learn

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        for item in inputs.metadata.to_json_structure():
            if item.get("metadata",{}).get("location_base_uris") is not None:
                base_uri = item.get("metadata",{}).get("location_base_uris")[0]
                break

        p_parts = base_uri[7:-1].split("/")
        #print(p_parts,file=sys.__stdout__)
        self._p = "/".join(p_parts[:5] + ["LL1_penn_fudan_pedestrian_dataset", "media"])
        #print(self._p, file=sys.__stdout__)
        #print(inputs.head(), file=sys.__stdout__)
        #print("Input metadata", file=sys.__stdout__)
        #print(inputs.metadata.to_json_structure(), file=sys.__stdout__)
        #inputs is assuming our d3m dataframe from DatasetToDataframe
        self._train_images, self._train_lbl_bbox =  self._get_annotations_from_d3m_dataframe(inputs)

        images, lbl_bbox = self._train_images, self._train_lbl_bbox
        #print(f"(ALL_IMAGES): {images}",file=sys.__stdout__)
        img2bbox = dict(zip(images, lbl_bbox))
        get_y_func = lambda o:img2bbox[o.name]
        def get_data(bs, size, training=True):

            #self._p = "/" + "/".join(self.hyperparams['dataset'].metadata.to_json_structure()[0]['metadata']['location_uris'][0].split("/")[3:-1]) + "/media"
            print(f"FILEPATH: {self._p}",file=sys.__stdout__)
            all_p = [file for file in os.listdir(self._p) if file.endswith('.png') or file.endswith('.jpg') or file.endswith('.JPG')]
            #valid_imgs = [f for f in all_p if f not in images]
            src = ObjectItemList.from_folder(self._p)
            #print(f"(ALL_IMAGES): {[f.name for f in src.items]}",file=sys.__stdout__)
            valid_imgs = [f for f in src.items if f.name not in images]
            print(f"len items before: {len(src.items)}", file=sys.__stdout__)
            src.items = np.array([f for f in src.items if f.name in images])
            print(f"len items after: {len(src.items)}", file=sys.__stdout__)
            #src = ObjectItemList.from_df(df=inputs, path=p, cols="image")
            #print(valid_imgs)
            #print(len(valid_imgs))
            #src = src.split_by_files(valid_names=valid_imgs)
            src = src.split_none()
            src = src.label_from_func(get_y_func)

            src = src.transform(get_transforms(), size=size, tfm_y=True)
            #src = src.add_test(items=valid_imgs, label=None)
            return src.databunch(path=self._p, bs=bs, collate_fn=bb_pad_collate, num_workers=0)

        self._data = get_data(self.hyperparams["batch_size"], self.hyperparams["img_size"], training=True)

        return

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        def cthw2tlbr(boxes):
            "Convert center/size format `boxes` to top/left bottom/right corners."
            top_left = boxes[:,:2] - boxes[:,2:]/2
            bot_right = boxes[:,:2] + boxes[:,2:]/2
            return torch.cat([top_left, bot_right], 1)

        def tlbr2cthw(boxes):
            "Convert top/left bottom/right format `boxes` to center/size corners."
            center = (boxes[:,:2] + boxes[:,2:])/2
            sizes = boxes[:,2:] - boxes[:,:2]
            return torch.cat([center, sizes], 1)

        def activ_to_bbox(acts, anchors, flatten=True):
            "Extrapolate bounding boxes on anchors from the model activations."
            if flatten:
                acts.mul_(acts.new_tensor([[0.1, 0.1, 0.2, 0.2]])) #Can't remember where those scales come from, but they help regularize
                centers = anchors[...,2:] * acts[...,:2] + anchors[...,:2]
                sizes = anchors[...,2:] * torch.exp(acts[...,:2])
                return torch.cat([centers, sizes], -1)
            else: return [activ_to_bbox(act,anc) for act,anc in zip(acts, anchors)]
            return res

        def create_grid(size):
            "Create a grid of a given `size`."
            H, W = size if is_tuple(size) else (size,size)
            grid = FloatTensor(H, W, 2)
            linear_points = torch.linspace(-1+1/W, 1-1/W, W) if W > 1 else tensor([0.])
            grid[:, :, 1] = torch.ger(torch.ones(H), linear_points).expand_as(grid[:, :, 0])
            linear_points = torch.linspace(-1+1/H, 1-1/H, H) if H > 1 else tensor([0.])
            grid[:, :, 0] = torch.ger(linear_points, torch.ones(W)).expand_as(grid[:, :, 1])
            return grid.view(-1,2)

        def create_anchors(sizes, ratios, scales, flatten=True):
            "Create anchor of `sizes`, `ratios` and `scales`."
            aspects = [[[s*math.sqrt(r), s*math.sqrt(1/r)] for s in scales] for r in ratios]
            aspects = torch.tensor(aspects).view(-1,2)
            anchors = []
            for h,w in sizes:
                #4 here to have the anchors overlap.
                sized_aspects = 4 * (aspects * torch.tensor([2/h,2/w])).unsqueeze(0)
                base_grid = create_grid((h,w)).unsqueeze(1)
                n,a = base_grid.size(0),aspects.size(0)
                ancs = torch.cat([base_grid.expand(n,a,2), sized_aspects.expand(n,a,2)], 2)
                anchors.append(ancs.view(h,w,a,4))
            return torch.cat([anc.view(-1,4) for anc in anchors],0) if flatten else anchors

        def intersection(anchors, targets):
            "Compute the sizes of the intersections of `anchors` by `targets`."
            ancs, tgts = cthw2tlbr(anchors), cthw2tlbr(targets)
            a, t = ancs.size(0), tgts.size(0)
            ancs, tgts = ancs.unsqueeze(1).expand(a,t,4), tgts.unsqueeze(0).expand(a,t,4)
            top_left_i = torch.max(ancs[...,:2], tgts[...,:2])
            bot_right_i = torch.min(ancs[...,2:], tgts[...,2:])
            sizes = torch.clamp(bot_right_i - top_left_i, min=0)
            return sizes[...,0] * sizes[...,1]

        def nms(boxes, scores, thresh=self.hyperparams["nms_threshold"]):
            idx_sort = scores.argsort(descending=True)
            boxes, scores = boxes[idx_sort], scores[idx_sort]
            to_keep, indexes = [], torch.LongTensor(range_of(scores))
            while len(scores) > 0:
                to_keep.append(idx_sort[indexes[0]])
                iou_vals = IoU_values(boxes, boxes[:1]).squeeze()
                mask_keep = iou_vals < thresh
                if len(mask_keep.nonzero()) == 0: break
                boxes, scores, indexes = boxes[mask_keep], scores[mask_keep], indexes[mask_keep]
            return LongTensor(to_keep)

        def IoU_values(anchors, targets):
            "Compute the IoU values of `anchors` by `targets`."
            inter = intersection(anchors, targets)
            anc_sz, tgt_sz = anchors[:,2] * anchors[:,3], targets[:,2] * targets[:,3]
            union = anc_sz.unsqueeze(1) + tgt_sz.unsqueeze(0) - inter
            return inter/(union+1e-8)

        def process_output(output, i, dataset, detect_thresh=0.25):
            "Process `output[i]` and return the predicted bboxes above `detect_thresh`."
            clas_pred,bbox_pred,sizes = output[0][0], output[1][0], output[2]
            anchors = create_anchors(sizes, self._ratios, self._scales).to(clas_pred.device)
            bbox_pred = activ_to_bbox(bbox_pred, anchors)
            clas_pred = torch.sigmoid(clas_pred)
            detect_mask = clas_pred.max(1)[0] > detect_thresh
            bbox_pred, clas_pred = bbox_pred[detect_mask], clas_pred[detect_mask]
            bbox_pred = tlbr2cthw(torch.clamp(cthw2tlbr(bbox_pred), min=-1, max=1))

            if clas_pred.shape[0] == 0:
                return None, None, None
            scores, preds = clas_pred.max(1)

            to_keep = nms(bbox_pred, scores)
            bbox_pred, preds, scores = bbox_pred[to_keep].cpu(), preds[to_keep].cpu(), scores[to_keep].cpu()
            t_sz = torch.Tensor([*dataset[i].size])[None].float()
            bbox_pred[:,:2] = bbox_pred[:,:2] - bbox_pred[:,2:]/2
            bbox_pred[:,:2] = (bbox_pred[:,:2] + 1) * t_sz/2
            bbox_pred[:,2:] = bbox_pred[:,2:] * t_sz

            return bbox_pred, scores, preds

        def center_size_to_coords(bbox):
            #top left
            y1 = round(float(bbox[0] - bbox[2]/2),2)
            x1 = round(float(bbox[1] - bbox[3]/2),2)
            #top right
            y2 = round(float(bbox[0] - bbox[2]/2),2)
            x2 = round(float(bbox[1] + bbox[3]/2),2)
            #bottom right
            y3 = round(float(bbox[0] + bbox[2]/2),2)
            x3 = round(float(bbox[1] + bbox[3]/2),2)
            #bottom left
            y4 = round(float(bbox[0] + bbox[2]/2),2)
            x4 = round(float(bbox[1] - bbox[3]/2),2)
            return f"{x1},{y1},{x2},{y2},{x3},{y3},{x4},{y4}"


        # Forward pass through our model of input images and calculate our bbox's, scores, and preds and then
        # go through consolidation via nms and score confidence
        test_dataset = ImageList.from_folder(self._p)
        produce_imgs = inputs[self._image_col_name].unique().tolist()
        d3m_indices = inputs["d3mIndex"].unique().tolist()
        #print(f"(PRODUCE): {produce_imgs[:10]}",file=sys.__stdout__)
        #print(f"(PRODUCE) len input images: {len(produce_imgs)}", file=sys.__stdout__)
        print(f"(PRODUCE) len items before: {len(test_dataset.items)}", file=sys.__stdout__)
        test_dataset.items = np.array([f for f in test_dataset.items if f.name in produce_imgs])
        print(f"(PRODUCE) len items after: {len(test_dataset.items)}", file=sys.__stdout__)
        output_df = DataFrame(columns = ["d3mIndex","image","class","confidence","bounding_box"])

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey')
        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/Integer')

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/Text')

        #output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 3), 'https://metadata.datadrivendiscovery.org/types/Confidence')

        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/BoundingPolygon')
        output_df.metadata = output_df.metadata.add_semantic_type((metadata_module.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        print(test_dataset)
        print(f"Predicting: {len(test_dataset)} images")
        for i in range(len(test_dataset.items)):
            print(i,file=sys.__stdout__)
            #print(test_dataset[i])
            inp = test_dataset[i].data[None]
            fastai.torch_core.defaults.device = torch.device('cpu')
            # if self.hyperparams['use_gpu']:
            #     inp = inp.cuda()
            self._learn.model = self._learn.model.cpu()
            outputs = self._learn.model(inp)
            #print(outputs)
            bbox_pred, scores, preds = process_output(outputs, i, test_dataset, detect_thresh=self.hyperparams["detect_threshold"])

            sub = inputs[inputs[self._image_col_name] == test_dataset.items[i].name]
            d3mIndex = sub.iloc[0]["d3mIndex"]
            image = test_dataset.items[i].name

            if bbox_pred is not None:
                for j in range(len(scores)):
                    pred_class = self._learn.data.classes[preds[j]+1]
                    pred_confidence = float(scores[j].data)
                    pred_bbox = center_size_to_coords(bbox_pred[j])

                    data = {"d3mIndex":d3mIndex, "image":image, "class":pred_class, "confidence":pred_confidence, "bounding_box":pred_bbox}
                    #print(data)
                    output_df = output_df.append(pd.Series(data), ignore_index=True)
            else:
                data = {"d3mIndex":d3mIndex, "image":image, "class":"n/a", "confidence":0, "bounding_box":"0,0,0,0,0,0,0,0"}
                output_df = output_df.append(pd.Series(data), ignore_index=True)

        missing_d3mIndices = list(set(d3m_indices).difference(set(output_df["d3mIndex"].unique().tolist())))
        for idx in missing_d3mIndices:
            data = {"d3mIndex":idx, "image":"n/a", "class":"n/a", "confidence":0, "bounding_box":"0,0,0,0,0,0,0,0"}
            output_df = output_df.append(pd.Series(data), ignore_index=True)
        print(output_df,file=sys.__stdout__)

        return CallResult(output_df)

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        def retina_net_split(model):
            groups = [list(model.encoder.children())[:6], list(model.encoder.children())[6:]]
            return groups + [list(model.children())[1:]]

        self._learn = self._create_learner()
        self._learn = self._learn.split(retina_net_split)
        self._learn.freeze()

        if iterations is None:
            iterations = 5
        pre = math.ceil(iterations / 4)
        post = iterations - pre

        self._learn.fit_one_cycle(pre, self.hyperparams["learning_rate"])
        self._learn.unfreeze()
        self._learn.fit_one_cycle(post, self.hyperparams["learning_rate"])

        return CallResult(None)

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:

        if iterations is None:
            iterations = 1
        self._learn.fit_one_cycle(iterations, self.hyperparams["learning_rate"])

        return CallResult(None)


    def get_params(self) -> Params:
        pass

    def set_params(self, *, params: Params) -> None:
        self._clf.cluster_centers = params.cluster_centers
