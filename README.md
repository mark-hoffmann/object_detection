# Object Detection Primitives

Primitives to solve an object detection problem.

# Instructions for testing and submitting

## Testing

- Launch GPU instance

- Open up port forwarded ssh connection to remote GPU instance

- Start newest docker image

`nvidia-docker run -itd -p 8887:8887 -v /home/ubuntu/d3m:/d3m <image_id>`

- Connect into started container

`docker exec -it <container_id> bash`

- (Optional) If you want to test interactively with a notebook as well you can start a port forwarded Jupyter notebok session:

```
pip install jupyter
pip install fastai

jupyter notebook --port 8887 --ip=0.0.0.0 --allow-root
```

- Make sure packages are pip installed in editable mode

```
pip install -e .
```

- cd into `object_detection` and run the metadata pipeline check:

```
python3 -m d3m.metadata.pipeline -c /d3m/object_detection/pipelines/object_detection.retina_net.RetinaNet/pipelines/2aa021cf-4dfc-4d46-82bc-738017555bd2.json
```

- To run the actual pipeline:

```
python3 -m d3m.runtime --strict-resolving -d /d3m/datasets/seed_datasets_current fit-score -n /d3m/object_detection/scoring.yml -m /d3m/object_detection/pipelines/object_detection.retina_net.RetinaNet/pipelines/2aa021cf-4dfc-4d46-82bc-738017555bd2.meta -p /d3m/object_detection/pipelines/object_detection.retina_net.RetinaNet/pipeliens/2aa021cf-4dfc-4d46-82bc-738017555bd2.json
```


## Submitting

Make sure to push to master BEFORE running the following:

```
python3 -m d3m.index describe -i 4 d3m.primitives.object_detection.retina_net.ObjectDetection > primitive.json
```

Make pull request of the `primitive.json` and accompanyting `pipelines` directory into the `primitives_repo`
